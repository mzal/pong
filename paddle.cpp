#include "paddle.hpp"

Paddle::Paddle(int len, int start_x, int start_y, int brd_y, int vel){
    length = len;
    position_x = start_x;
    position_y = start_y;
    velocity = vel;
    board_y = brd_y;
}

sf::Vector2f Paddle::getPosition(void){
    return sf::Vector2f(position_x, position_y);
}

void Paddle::movePaddle(bool direction){
    if (direction && (position_y - length/2 - velocity > 0) ) position_y -= velocity;
    if (!direction && (position_y + length/2 + velocity < board_y) ) position_y += velocity;
    //TODO: fix the logic here sometime
}
