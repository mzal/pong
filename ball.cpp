#include <SFML/System.hpp>
#include <cmath>
#include "ball.hpp"

Ball::Ball(int rad, int start_x, int start_y, int brd_x, int brd_y, int vel, float tan, bool dir){
    radius = rad;
    position_x = start_x;
    position_y = start_y;
    board_x = brd_x;
    board_y = brd_y;
    velocity = vel;
    direction_x = dir;
    tangent = tan;
    this->calculateGrowth(tangent);
}

void Ball::moveBall(void){
    position_x += x_growth;
    position_y -= y_growth;
}

sf::Vector2f Ball::getPosition(void){
    return sf::Vector2f(position_x, position_y);
}

void Ball::calculateGrowth(float tan){
    if (direction_x){
        x_growth = velocity / sqrt(tan * tan + 1);
        if (direction_y)
            y_growth = tan * x_growth;
        else
            y_growth = - tan * x_growth;
    }
    else {
        x_growth = - velocity / sqrt(tan * tan + 1);
        //y_growth = - tan * x_growth;
        if (direction_y)
            y_growth = - tan * x_growth;
        else
            y_growth = tan * x_growth;
    }
}

int Ball::detectCollision(Paddle* paddle1, Paddle* paddle2){
    /*
    0 no collision
    1 collision with wall 1
    2 collision with wall 2
    */
    if (position_x + x_growth - radius < 0){
        return 1;
    }
    if (position_x + x_growth + radius > board_x){
        return 2;
    }
    if (position_y + y_growth + radius > board_y){
        direction_y = true;
        this->calculateGrowth(tangent);
    }
    if (position_y + y_growth - radius < 0){
        direction_y = false;
        this->calculateGrowth(tangent);
    }
    return 0;
}
