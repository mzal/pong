#ifndef _BALL_H
#define _BALL_H
#include <SFML/System.hpp>
#include "paddle.hpp"

class Ball {
    public:
        Ball(int rad, int start_x, int start_y, int brd_x, int brd_y, int vel, float tan, bool dir);
        int radius;
        void moveBall(void);
        void calculateGrowth(float tan);
        sf::Vector2f getPosition(void);
        int detectCollision(Paddle* paddle1, Paddle* paddle2);
    private:
        int velocity,
            position_x,
            position_y,
            board_x,
            board_y,
            x_growth,
            y_growth;
        float tangent;
        bool direction_x,   //true for Right, false for Left
            direction_y = true;   //true for Up, false for Down
};

#endif // _BALL_H
