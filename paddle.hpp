#ifndef _PADDLE_H
#define _PADDLE_H
#include <SFML/System.hpp>

class Paddle {
    public:
        Paddle(int len, int start_x, int start_y, int brd_y, int vel);
        int length;
        sf::Vector2f getPosition(void);
        void movePaddle(bool direction); //true for Up, false for Down
    private:
        int position_x,
            position_y,
            velocity,
            board_y;
};

#endif // _PADDLE_H
