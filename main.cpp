#include <SFML/Graphics.hpp>
#include "paddle.hpp"
#include "ball.hpp"
#define WINDOW_X 800
#define WINDOW_Y 600
#define FRAMERATE 0.05
#define PADDLE_SIZE_X 20
#define PADDLE_SIZE_Y 100
#define PADDLE_POS_X 50
#define PADDLE_VELOCITY 5
#define BALL_RADIUS 10
#define BALL_VELOCITY 15

int main(){
    sf::RenderWindow window(sf::VideoMode(800,600),"PONG");
    sf::Time delay = sf::seconds(FRAMERATE);

    Paddle* paddle1 = new Paddle(PADDLE_SIZE_Y, PADDLE_POS_X, WINDOW_Y/2, WINDOW_Y, PADDLE_VELOCITY);
    Paddle* paddle2 = new Paddle(PADDLE_SIZE_Y, WINDOW_X - PADDLE_POS_X, WINDOW_Y/2, WINDOW_Y, PADDLE_VELOCITY);
    Ball* ball = new Ball(BALL_RADIUS, WINDOW_X/2, WINDOW_Y/2, WINDOW_X, WINDOW_Y, BALL_VELOCITY, 1.7, false);
    while(window.isOpen()){
        sf::Event event;
        while (window.pollEvent(event)){
            if (event.type == sf::Event::Closed)
                window.close();
        }

        if(sf::Keyboard::isKeyPressed(sf::Keyboard::W))
            paddle1->movePaddle(true);
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::S))
            paddle1->movePaddle(false);
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
            paddle2->movePaddle(true);
        if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
            paddle2->movePaddle(false);

        ball->moveBall();
        ball->detectCollision(paddle1, paddle2);

        window.clear(sf::Color::Black);

        sf::RectangleShape paddle_shape;
        paddle_shape.setSize(sf::Vector2f(PADDLE_SIZE_X,paddle1->length));
        paddle_shape.setOrigin(sf::Vector2f(paddle_shape.getSize().x/2, paddle_shape.getSize().y/2));
        paddle_shape.setPosition(paddle1->getPosition());
        window.draw(paddle_shape);

        paddle_shape.setSize(sf::Vector2f(PADDLE_SIZE_X,paddle2->length));
        paddle_shape.setOrigin(sf::Vector2f(paddle_shape.getSize().x/2, paddle_shape.getSize().y/2));
        paddle_shape.setPosition(paddle2->getPosition());
        window.draw(paddle_shape);

        sf::CircleShape ball_shape;
        ball_shape.setRadius(ball->radius);
        ball_shape.setOrigin(ball_shape.getLocalBounds().width/2, ball_shape.getLocalBounds().height/2);
        ball_shape.setPosition(ball->getPosition());
        window.draw(ball_shape);

        window.display();
        sf::sleep(delay);
    }


}
